import { getPromoFolders } from "../../server/promo-folder"


export default async function handler(req, res) {

    const menus = await getPromoFolders();

    res.status(200).json(menus)
}