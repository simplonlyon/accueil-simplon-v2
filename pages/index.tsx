import Layout from '../components/Layout'
import Image from 'next/image';

const IndexPage = () => (
  <Layout title="Bienvenue sur Simplon Lyon">
    <main id="index">
        <section id="gris">
            <h1><Image layout="fixed" width="60" height="60" src="/img/illustration.png"/>Simplon : Fabrique de développeur.euse.s sociale et solidaire</h1>
        </section>
        <h2>Recrutez un.e développeur.euse <br/><span>motivé.e</span></h2>
        
        <a id="catch" href="catalogue.php?promo=12">Voir les portfolios !</a>
        <p>Les apprenant.e.s de Simplon sont sélectionné.e.s avec soin sur le critère de leur motivation et de leurs capacités à apprendre et travailler en équipe. Leurs implication et compétences sont garanties par la maison !</p>
        <h3>Pour plus d'information sur les programmes et contact :</h3>
        <section id="boutons">
            <a href="https://auvergnerhonealpes.simplon.co/">Consulter le site Simplon Auverne Rhône Alpes</a>
            
        </section>
    </main>
  </Layout>
)

export default IndexPage

export async function getStaticProps() {
  console.log(process.env.PROMO_FOLDER);
  return {
    props:{}
  }
}
