import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import Link from 'next/link'
import React from 'react'

const ActiveLink = ({ children, activeClassName = null, ...props }) => {
  const { asPath } = useRouter()

  // pages/index.js will be matched via props.href
  // pages/about.js will be matched via props.href
  // pages/[slug].js will be matched via props.as
  const className =
    asPath === props.href || asPath === props.as
      ? `${activeClassName}`.trim()
      : ''

  return (
    <Link href={props.href} {...props}>
      <a className={className}>
        {children}
      </a>
    </Link>
  )
}

ActiveLink.propTypes = {
  activeClassName: PropTypes.string,
}

export default ActiveLink