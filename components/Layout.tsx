import React, { ReactNode } from 'react'
import ActiveLink from './ActiveLink';
import Image from 'next/image';
import Head from 'next/head'
import { Nav } from './Nav';


type Props = {
  children?: ReactNode
  title?: string
}

const Layout = ({ children, title = 'Simplon Lyon - Accueil' }: Props) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
      <meta name="title" content="Simplon Lyon" />
      <meta name="description" content="Simplon Lyon : une fabrique de developpeur.euse sociale et solidaire. Venez recruter votre propre dev motivé.e !" />
      <meta name="author" content="Clémence Laffont" />
      <link rel="stylesheet" type="text/css" href="css/style.css" />
      <link rel="stylesheet" type="text/css" href="css/modif.css" />
      <link rel="shortcut icon" href="img/avatar.png" />
      <script src="https://use.fontawesome.com/0eccec29f2.js"></script>
      <meta property="og:image" content="https://www.simplonlyon.fr/AccueilSimplon/img/avatar.png" />
      <meta property="og:image:width" content="250" />
      <meta property="og:image:height" content="250" />
      <meta property="og:title" content="Simplon Lyon" />
      <meta property="og:description" content="Simplon Lyon : une fabrique de developpeur.euse social et solidaire. Venez recruter votre propre dev motivé.e !" />
      <link rel="image_src" href="https://www.simplonlyon.fr/img/avatar.png" />
    </Head>
    <header className="nav">
    <div>
          <ActiveLink href="/">
              <Image layout="intrinsic" height="90" width="245" src="/img/logo-simplonlyon.jpg" alt="logo de Simplon Lyon"/>
          </ActiveLink>
        <svg id="deroulant" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 100 125" x="0px" y="0px"><title>122all</title><rect x="16.56" y="45" width="66.88" height="10"/><rect x="16.56" y="21.92" width="66.88" height="10"/><rect x="16.56" y="68.08" width="66.88" height="10"/></svg>
    </div>
    <Nav/>
    <script type="text/javascript" src="js/menuMobile.js"></script>
</header>

    {children}
    <footer className="sociaux">
        <a href="https://github.com/simplon-lyon" aria-label="Lien vers le compte github de Simplon Lyon" title="Lien vers le compte github de Simplon Lyon"><i className="fa fa-github fa-3x" aria-hidden="true"></i></a>
        <a href="https://twitter.com/SimplonLyon" aria-label="Lien vers la page Twitter de Simplon Lyon" title="Lien vers la page Twitter de Simplon Lyon"><i className="fa fa-twitter fa-3x" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/showcase/simplon-auvergne-rh%C3%B4ne-alpes/?viewAsMember=true" aria-label="Lien vers le compte LinkedIn company de Simplon.co" title="Lien vers le compte LinkedIn company de Simplon.co"><i className="fa fa-linkedin fa-3x" aria-hidden="true"></i></a>
        <a href="https://www.facebook.com/SimplonLyon/?ref=br_rs" aria-label="Lien vers la page facebook de Simplon Lyon" title="Lien vers la page facebook de Simplon Lyon"><i className="fa fa-facebook fa-3x" aria-hidden="true"></i></a>
    </footer>
  </div>
)

export default Layout
