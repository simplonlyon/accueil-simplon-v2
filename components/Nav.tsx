
import { useState } from "react"
import ActiveLink from "./ActiveLink"


export const Nav = () => {
    const [promos, setPromos] = useState([])


    fetch('/api/menu').then(response => response.json()).then(data => setPromos(data));

    return (
        <nav>
            <ul>
                <li>
                    <ActiveLink activeClassName="page-active" href="/">Accueil</ActiveLink>
                </li>
                <li>
                    <ActiveLink activeClassName="page-active" href="/catalogue">Portfolios</ActiveLink>
                    <ul className="sub_menu">
                        {promos.map(promo => 
                            <li key={promo.slug}><ActiveLink href={'/catalogue?promo='+promo.slug}>{promo.name}</ActiveLink></li>
                        )}

                    </ul>
                </li>
            </ul>
        </nav>
    )
}

