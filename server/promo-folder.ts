import fs from 'fs';

export function getPromoFolders() {
    return new Promise((resolve, reject) => {

        fs.readdir(process.env.PROMO_FOLDER, (err, files) => {
            const folders = files.filter(item => item.startsWith('promo'))
                                .map(item => ({name: displayFolder(item), slug:item}));

            resolve(folders);
        })
    })
}


function displayFolder(folder:string):string {
    return folder.substr(0, 1).toUpperCase() + folder.substr(1, 4) +' '+folder.substr(5, folder.length);
}